package com.example.assignment14

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.assignment14.adapter.UserAdapter
import com.example.assignment14.adapter.UserLoadingAdapter
import com.example.assignment14.databinding.FragmentUserBinding
import com.example.assignment14.viewmodel.UserInfoViewModel


class UserFragment : BaseFragment<FragmentUserBinding>(FragmentUserBinding::inflate){


    private val userInfoViewModel : UserInfoViewModel by viewModels()
    lateinit var userAdapter: UserAdapter

    override fun start() {
        setUpRecycler()
    }

    private fun setUpRecycler()
    {
        binding.rvUser.apply {
            userAdapter = UserAdapter()
            adapter = userAdapter
            layoutManager = LinearLayoutManager(context)

        }

        lifecycleScope.launchWhenCreated {
            userInfoViewModel.fetchUsers().observe(viewLifecycleOwner,{
                userAdapter.submitData(lifecycle,it)
            })
        }


        binding.rvUser.adapter = userAdapter.withLoadStateHeaderAndFooter(
            header = UserLoadingAdapter { userAdapter.retry() },
            footer = UserLoadingAdapter { userAdapter.retry() }
        )

    }

}