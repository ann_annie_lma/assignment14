package com.example.assignment14

import androidx.paging.PagingSource
import androidx.paging.PagingState
import com.example.assignment14.model.User
import com.example.assignment14.network.ApiService
import com.example.assignment14.network.NetworkClient
import retrofit2.HttpException
import java.io.IOException

class UserPagingSource : PagingSource<Int, User.Data>() {

    override fun getRefreshKey(state: PagingState<Int, User.Data>): Int? {
        return null
    }

    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, User.Data> {

       return try {
            val page = params.key ?: 1
            val userResponse = NetworkClient.api.getUsersInfo(page)
            val resultBody = userResponse.body()

            if(userResponse.isSuccessful && resultBody!= null)
            {
                var nextPage:Int? = null
                var previousPage:Int? = null


                if(page < resultBody.totalPages)
                {
                    nextPage = page + 1
                }

                if(page != 1)
                {
                    previousPage = page - 1
                }

                LoadResult.Page(resultBody.data,previousPage,nextPage)
            }
            else
            {
                LoadResult.Error(Throwable())
            }

        } catch (exception: IOException) {
            return LoadResult.Error(exception)
        } catch (exception: HttpException) {
            return LoadResult.Error(exception)
        }

    }

}