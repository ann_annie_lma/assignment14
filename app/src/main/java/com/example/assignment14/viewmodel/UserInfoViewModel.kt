package com.example.assignment14.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.*
import com.example.assignment14.UserPagingSource
import com.example.assignment14.model.User

class UserInfoViewModel : ViewModel() {

    fun fetchUsers() = Pager(config = PagingConfig(pageSize = 1),pagingSourceFactory = {UserPagingSource()}).liveData.cachedIn(viewModelScope)

}