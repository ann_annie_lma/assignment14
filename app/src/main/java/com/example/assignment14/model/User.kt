package com.example.assignment14.model

import com.google.gson.annotations.SerializedName

data class User(
    val `data`: List<Data>,
    val page: Int,
    @SerializedName("per_page")
    val perPage: Int,
    val total: Int,
    @SerializedName("total_pages")
    val totalPages: Int
)
{
    data class Data(
        val avatar: String,
        val email: String,
        @SerializedName("first_name")
        val firstName: String,
        val id: Int,
        @SerializedName("last_name")
        val lastName: String
    )
}