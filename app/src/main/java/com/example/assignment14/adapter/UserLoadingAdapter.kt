package com.example.assignment14.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.paging.LoadState
import androidx.paging.LoadStateAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.assignment14.R
import com.example.assignment14.databinding.ItemLoadingStateBinding
import com.example.assignment14.databinding.UserItemBinding

class UserLoadingAdapter(private val retry: () -> Unit) :
    LoadStateAdapter<UserLoadingAdapter.LoadingStateViewHolder>() {

    class LoadingStateViewHolder(val binding: ItemLoadingStateBinding, retry: () -> Unit) :
        RecyclerView.ViewHolder(binding.root) {

        init {
            binding.btnRetry.setOnClickListener {
                retry()
            }
        }

        fun bindState(loadState: LoadState) {
            if (loadState is LoadState.Error) {
                binding.tvErrorMessage.text = loadState.error.localizedMessage
            }

            binding.progressBar.isVisible = loadState is LoadState.Loading
            binding.tvErrorMessage.isVisible = loadState !is LoadState.Loading
            binding.btnRetry.isVisible = loadState !is LoadState.Loading
        }
    }

    override fun onBindViewHolder(holder: LoadingStateViewHolder, loadState: LoadState) {
        holder.bindState(loadState)
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        loadState: LoadState
    ): LoadingStateViewHolder {

        return LoadingStateViewHolder(
            ItemLoadingStateBinding.inflate(LayoutInflater.from(parent.context),
            parent,
            false),retry)
    }
}