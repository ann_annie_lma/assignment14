package com.example.assignment14.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.paging.DifferCallback
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import androidx.viewbinding.ViewBinding
import com.bumptech.glide.Glide
import com.example.assignment14.R
import com.example.assignment14.databinding.UserItemBinding
import com.example.assignment14.model.User

class UserAdapter() : PagingDataAdapter<User.Data,UserAdapter.UserViewHolder>(DiffCallBack()) {

    inner class UserViewHolder(val binding: UserItemBinding) : RecyclerView.ViewHolder(binding.root)
    {
        fun bind(user:User.Data)
        {
            binding.tvFirstName.text = user.firstName
            binding.tvLastName.text = user.lastName
            binding.tvEmail.text = user.email

            Glide.with(binding.ivAvatar.getContext())  // glide with this view
                .load(user.avatar)
                .override(250, 250) // set this size
                .placeholder(R.mipmap.ic_launcher) // default placeHolder image
                .error(R.mipmap.ic_launcher)
                .centerCrop()// use this image if faile
                .into(binding.ivAvatar)


        }
    }

    override fun onBindViewHolder(holder: UserViewHolder, position: Int) {
       holder.bind(getItem(position)!!)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): UserViewHolder {
        return UserViewHolder(UserItemBinding.inflate(LayoutInflater.from(parent.context),
        parent,
        false))
    }

    class DiffCallBack : DiffUtil.ItemCallback<User.Data>()
    {
        override fun areItemsTheSame(oldItem: User.Data, newItem: User.Data): Boolean {
            return oldItem.id == newItem.id

        }

        override fun areContentsTheSame(oldItem: User.Data, newItem: User.Data): Boolean {
            return oldItem == newItem
        }

    }

}