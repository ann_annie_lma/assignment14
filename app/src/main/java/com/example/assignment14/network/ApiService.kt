package com.example.assignment14.network

import com.example.assignment14.model.User
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiService {

    @GET("users")
    suspend fun getUsersInfo(@Query("page")page : Int):Response<User>
}